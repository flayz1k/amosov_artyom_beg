from RationalFraction import RationalFraction

if __name__ == '__main__':
    rf_one = RationalFraction(3, 7)
    rf_two = RationalFraction(5, 9)
    print(rf_one)
    print(rf_two)
    print(rf_one + rf_two)
    print(rf_one - rf_two)
    print(rf_one == rf_two)
