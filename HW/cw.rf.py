class RationalFraction


    def __init__(self, numerator: int = 1, denominator: int = 1):
    if denominator == 0:
        raise Exception
    self.numerator = numerator
    self.denominator = denominator

    def __sub__(self, other):
        answer = RationalFraction(denominator=(self.denominator * other.denominator),
                                  numerator=((self.numerator * other.denominator) - (
                                              other.numerator * self.denominator)))
        return answer

    def __isub__(self, other):
        self.numerator = (self.numerator * other.denominator) - (other.numerator * self.denominator)
        self.denominator = self.denominator * other.denominator

    def __add__(self, other):
        answer = RationalFraction(denominator=(self.denominator * other.denominator),
                                  numerator=((self.numerator * other.denominator) + (
                                              other.numerator * self.denominator)))

        return answer

    def __iadd__(self, other):
        self.numerator = (self.numerator * other.denominator) + (other.numerator * self.denominator)
        self.denominator = self.denominator * other.denominator

    def __str__(self):
        return "{}/{}".format(self.numerator, self.denominator)

    def __eq__(self, other):
        return self.numerator == other.numerator and self.denominator == other.denominator
